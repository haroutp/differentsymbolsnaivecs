﻿using System;
using System.Collections.Generic;

namespace DifferentSymbolsNaive
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        int differentSymbolsNaive(string s) {
            List<char> symbols = new List<char>();
            
            foreach(char item in s){
                if(!symbols.Contains(item)){
                    symbols.Add(item);
                }
            }
            
            return symbols.Count;
        }

    }
}
